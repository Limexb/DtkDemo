#ifndef WIDGET_H
#define WIDGET_H
#include <QWidget>
#include <DMainWindow>
#include <DGuiApplicationHelper>//用来适配深色模式
#include "widget.h"

#include <DLabel>
DWIDGET_USE_NAMESPACE

class MainWindow : public DMainWindow
{
    Q_OBJECT

public:
    MainWindow(DMainWindow *parent = nullptr);
    ~MainWindow();
private:
    QWidget *w=new QWidget; //w是窗口的用户区，应当是所有窗口中控件的父（不包含标题栏及其上边的控件）
    void setTheme(DGuiApplicationHelper::ColorType);
};

#endif // WIDGET_H
