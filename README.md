# DtkDemo

#### 介绍
> 简短却详细的Dtk开发实例

![dtkdemo](img/dtkdemo.png)

**程序中包含以下内容：**

- 创建一个DTK风格的程序窗口。
- 设置程序的一系列属性（包括图标，标题，版本号等）。
- 在窗口中添加相应控件。
- 在标题栏中添加相应控件。
- 在菜单中添加菜单项
- 浅色模式和深色模式的适配

**如果你想学习更多有关知识，可以访问以下网址：**

- DTK相关文档地址：https://linuxdeepin.github.io/dtk
- Deepin项目地址：https://github.com/linuxdeepin
- 社区项目投递地址：https://gitee.com/deepin-opensource

#### DTK依赖安装

```bash
sudo apt install libdtkcore-dev libdtkgui-dev libdtkwidget-dev libdtkwm-dev
```

